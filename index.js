/**
 * A simple generator for creating groups of simple memorable passwords and phrases.
 */

'use strict';

const png    = require( 'project-name-generator' );
const colors = require( 'colors' );

// Simple 3-word phrases
console.log( '\n', 'Phrases:'.bgBlue.bold, '\n' );

for ( let i = 0; i < 10; i++ ) {
  console.log( ( png({ words: 3 }).spaced ).green );
}

// Alliterative 3-word phrases
console.log( '\n', 'Alliterative phrases:'.bgBlue.bold, '\n' );

for ( let i = 0; i < 10; i++ ) {
  console.log( ( png({ words: 3, alliterative: true }).spaced ).green );
}

// Simple 2-word phrases
console.log( '\n', 'Pair phrases:'.bgBlue.bold, '\n' );

for ( let i = 0; i < 10; i++ ) {
  console.log( ( png({ words: 2 }).spaced ).green );
}

// Alliterative 2-word phrases
console.log( '\n', 'Alliterative pair phrases:'.bgBlue.bold, '\n' );

for ( let i = 0; i < 10; i++ ) {
  console.log( ( png({ words: 2, alliterative: true }).spaced ).green );
}

// Adjective dump
console.log( '\n', 'Adjectives:'.bgBlue.bold, '\n' );

let words = png({ words: 21 }).raw;

words.pop();

console.log( words.join( ' ' ).green );

// Noun dump
console.log( '\n', 'Nouns:'.bgBlue.bold, '\n' );

let nouns = [...Array( 20 )].map(() => {
  return png({ words: 1 }).spaced;
}, '' ).join( ' ' );

console.log( nouns.green );

console.log( '\n' );
